CREATE TABLE IF NOT EXISTS movie(
    movie_id int PRIMARY KEY auto_increment,
    movie_title VARCHAR(50),
    movie_release_date DATE,
    movie_time FLOAT,
    director_name VARCHAR(50)
);