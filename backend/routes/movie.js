const express = require("express")
const router = express.Router()
const utils = require("../utils")
const db = require("../db")
const {request, response} = require("express")

router.post("/addmovie",(request, response) => {
    const {movie_title, movie_release_date, movie_time, director_name} = request.body

    const query = `INSERT INTO movie (movie_title, movie_release_date, movie_time, director_name) VALUES ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`

    db.execute(query,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})
router.get("/getmoviebyname/:name",(request, response) => {
    const {movie_title} = request.params

    const query = `SELECT * FROM movie WHERE movie_title = '${movie_title}'`

    db.execute(query,(error,data) => {
        response.send(utils.createResult(error,data[0]))
    })
})
router.put("/update/:name",(request, response) => {
    const {movie_title} = request.params
    const {movie_release_date, movie_time} = request.body

    const query = `UPDATE movie SET movie_release_date = '${movie_release_date}', movie_time = '${movie_time}' WHERE movie_title = '${movie_title}' `

    db.execute(query,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})
router.delete("/delete/:name",(request, response) => {
    const {movie_title} = request.params
   

    const query = `DELETE FROM movie WHERE movie_title = '${movie_title}'`

    db.execute(query,(error,data) => {
        response.send(utils.createResult(error,data))
    })
})

module.exports = router

